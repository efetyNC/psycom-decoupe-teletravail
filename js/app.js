jQuery(document).ready(function ($) {

    /*** VARIABLES GENERALES ***/

    var $window = $(window);
    var $document = $(document);
    var header = $('header');
    var megamenu = $('.megamenu');
    var burger = $('.burger');
    var body = $('body');
    var footer = $('footer');
    var niveau1_li = $('.menu-desktop > li');
    var niveau1_a = $('.menu-desktop > li > a');
    var burger_ul = $('.menu-mobile ul');
    var burger_ul_3e_nv = $('.menu-mobile ul ul');
    var burger_ul_4e_nv = $('.menu-mobile ul ul ul');
    var burger_header = $('.burger-header');

    /*** FIN VARIABLES GENERALES ***/


    /*** IMPORT DES JS ***/
    function loadScript(path) {
        $.getScript(path, function (data, status, res) {
            if (res.status === 200) {
                var filename = path.replace(/^.*[\\\/]/, '');
                console.log(filename + ' chargé.');
            }
        })
    }


    /*** FIN D'IMPORT DES JS ***/


    /*** UTILITIES ***/

    //ScrollTo
    function scrollTo(target, condition) {
        if (condition.length) {
            var scroll = target.offset().top - $('header').height(); // prise en compte du header sticky - si sticky partiel, mettre la partie du header qui sera sticky à la place
            $("html, body").stop().animate({scrollTop: scroll}, 1500);
        }
    }

    // Add smooth scrolling to all links
    $("a[href^=\"#\"]").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it
            // takes to scroll to the specified area
            if ($('.liste-definitions').length > 0) {
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 200
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } else {
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 130
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            }
        } // End if
    });

    (function (document, history, location) {
        var HISTORY_SUPPORT = !!(history && history.pushState);

        if ($('.liste-definitions').length > 0) {
            var anchorScrolls = {
                ANCHOR_REGEX: /^#[^ ]+$/,
                OFFSET_HEIGHT_PX: 200,

                /**
                 * Establish events, and fix initial scroll position if a hash is provided.
                 */
                init: function () {
                    this.scrollToCurrent();
                    $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
                    $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
                },

                /**
                 * Return the offset amount to deduct from the normal scroll position.
                 * Modify as appropriate to allow for dynamic calculations
                 */
                getFixedOffset: function () {
                    return this.OFFSET_HEIGHT_PX;
                },

                /**
                 * If the provided href is an anchor which resolves to an element on the
                 * page, scroll to it.
                 * @param  {String} href
                 * @return {Boolean} - Was the href an anchor.
                 */
                scrollIfAnchor: function (href, pushToHistory) {
                    var match, anchorOffset;

                    if (!this.ANCHOR_REGEX.test(href)) {
                        return false;
                    }

                    match = document.getElementById(href.slice(1));

                    if (match) {
                        anchorOffset = $(match).offset().top - this.getFixedOffset();
                        $('html, body').animate({scrollTop: anchorOffset});

                        // Add the state to history as-per normal anchor links
                        if (HISTORY_SUPPORT && pushToHistory) {
                            history.pushState({}, document.title, location.pathname + href);
                        }
                    }

                    return !!match;
                },

                /**
                 * Attempt to scroll to the current location's hash.
                 */
                scrollToCurrent: function (e) {
                    if (this.scrollIfAnchor(window.location.hash) && e) {
                        e.preventDefault();
                    }
                },

                /**
                 * If the click event's target was an anchor, fix the scroll position.
                 */
                delegateAnchors: function (e) {
                    var elem = e.target;

                    if (this.scrollIfAnchor(elem.getAttribute('href'), true)) {
                        e.preventDefault();
                    }
                }
            };
        } else {
            var anchorScrolls = {
                ANCHOR_REGEX: /^#[^ ]+$/,
                OFFSET_HEIGHT_PX: 130,

                /**
                 * Establish events, and fix initial scroll position if a hash is provided.
                 */
                init: function () {
                    this.scrollToCurrent();
                    $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
                    $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
                },

                /**
                 * Return the offset amount to deduct from the normal scroll position.
                 * Modify as appropriate to allow for dynamic calculations
                 */
                getFixedOffset: function () {
                    return this.OFFSET_HEIGHT_PX;
                },

                /**
                 * If the provided href is an anchor which resolves to an element on the
                 * page, scroll to it.
                 * @param  {String} href
                 * @return {Boolean} - Was the href an anchor.
                 */
                scrollIfAnchor: function (href, pushToHistory) {
                    var match, anchorOffset;

                    if (!this.ANCHOR_REGEX.test(href)) {
                        return false;
                    }

                    match = document.getElementById(href.slice(1));

                    if (match) {
                        anchorOffset = $(match).offset().top - this.getFixedOffset();
                        $('html, body').animate({scrollTop: anchorOffset});

                        // Add the state to history as-per normal anchor links
                        if (HISTORY_SUPPORT && pushToHistory) {
                            history.pushState({}, document.title, location.pathname + href);
                        }
                    }

                    return !!match;
                },

                /**
                 * Attempt to scroll to the current location's hash.
                 */
                scrollToCurrent: function (e) {
                    if (this.scrollIfAnchor(window.location.hash) && e) {
                        e.preventDefault();
                    }
                },

                /**
                 * If the click event's target was an anchor, fix the scroll position.
                 */
                delegateAnchors: function (e) {
                    var elem = e.target;

                    if (this.scrollIfAnchor(elem.getAttribute('href'), true)) {
                        e.preventDefault();
                    }
                }
            };
        }


        $(document).ready($.proxy(anchorScrolls, 'init'));
    })(window.document, window.history, window.location);

    $.fn.lastWord = function () {
        var text = this.text().trim().split(" ");
        var last = text.pop();
        this.html(text.join(" ") + (text.length > 0 ? " <span class='last-word'>" + last + "</span>" : last));
    }

    $('.wysiwyg h3').each(function () {
        $(this).lastWord();
    });

    /*** Fin UTILITIES ***/

    /*** GLOBAL ***/
    // Tooltips trigger
    $('[data-toggle="tooltip"]').tooltip();

    // tableau responsive
    $('.wysiwyg table, .main-content table').wrap('<div class="tableau-responsive container"></div>');

    // loader sur les boutons
    $('.btn').on('click', function () {
        if (!$(this).hasClass('no-loader')) {
            if (!$(this).attr('target')) {
                $(this).width($(this).width()); // pour empêcher le changement de taille du bouton
                $(this).addClass('btn-loader');
                $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
            }
            // ajoutez la class "no-loader" aux boutons que vous souhaitez exclure
        }
    });

    // loader sur le bouton submit en input
    $('input[type="submit"]').on('click', function () {
        if ($(this).parents('form')[0].checkValidity()) {
            $(this).wrap('<span class="input-loader"></span>');
            $(this).parent().append('<div class="msg-envoi"><i class="fas fa-circle-notch fa-spin"></i><span>Veuillez patienter...</span></div>');
        }
    });

    // Scroll to error - gravity forms
    scrollTo($(".formulaire-anchor"), $(".gform_validation_error"), 1500); // ajoutez la classe "formulaire-anchor" dans les réglages du formulaire en back-office

    var navhei = $('.conteneur-logo').height();
    var navheix = navhei + 70;
    document.addEventListener('invalid', function (e) {
        $(e.target).addClass("invalid");

        $('html, body').stop().animate({
            scrollTop: ($($(".invalid")[0]).offset().top - navheix)
        }, 300);

        setTimeout(function () {
            $('.invalid').removeClass('invalid');
        }, 300);
    }, true);

    // Tarte au citron contraignant
    setTimeout(function () {
        if ($("#tarteaucitronAlertBig").css("display") === "block") {
            $("#tarteaucitronRoot").addClass("blur");
        }
    }, 1500);
    $document.on("click", "#tarteaucitronPersonalize", function () {
        $("#tarteaucitronRoot").removeClass("blur");
    });
    $document.on("click", "#tarteaucitronClosePanel", function () {
        window.location.reload();
    });


    /*** FIN GLOBAL ***/

    /*** HEADER ***/

        // Modification du header quand on est plus tout en haut de la page
    var windowPos = $window.scrollTop();
    if ($window.width() > 992) {
        if (windowPos > 10 && !header.hasClass('scrolling')) {
            header.addClass('scrolling');
        } else if (windowPos <= 10) {
            header.removeClass('scrolling');
        }
    }

    $window.on('scroll', function () {
        var windowPos = $window.scrollTop();
        if ($window.width() > 992) {
            if (windowPos > 10 && !header.hasClass('scrolling')) {
                header.addClass('scrolling');
            } else if (windowPos <= 10) {
                header.removeClass('scrolling');
            }
        }
    });

    $window.on('resize', function () {
        var windowPos = $window.scrollTop();
        if ($window.width() > 992) {
            if (windowPos > 10 && !header.hasClass('scrolling')) {
                header.addClass('scrolling');
            } else if (windowPos <= 10) {
                header.removeClass('scrolling');
            }
        } else if ($window.width() < 992 && header.hasClass('scrolling')) {
            header.removeClass('scrolling');
        }
    });

    $('header').addClass('loaded');

    /*** FIN HEADER ***/


    /*** MEGAMENU ***/


    /** Mégamenu au clic **/
    // niveau1_a.on('click', function(event){
    //     event.preventDefault();
    //     if($(this).parent().hasClass('active')){
    //         niveau1_li.removeClass('active');
    //     } else {
    //         niveau1_li.removeClass('active');
    //         $(this).parent().addClass('active');
    //     }
    // });


    /** Mégamenu au hover **/
    niveau1_a.on('mouseover', function () {
        niveau1_li.removeClass('active');
        $(this).parent().addClass('active');
    });
    $('.menu-desktop').on('mouseleave', function () {
        niveau1_li.removeClass('active');
    });


    /*** FIN MEGAMENU ***/

    /*** MENU BURGER ***/

    // ouverture/fermeture du burger
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
        if (burger.hasClass('active')) {
            // fermeture du burger
            burger.addClass('is-closing');
            setTimeout(function () {
                burger.removeClass('active').removeClass('is-closing');
                burger_ul.removeClass('active');
                burger_header.removeClass('active');
                burger_header.find('.conteneur-rappel-menu').html('<span class="rappel-menu">Nos rubriques</span>');
                body.removeClass('no-scroll');
            }, 500);
        } else {
            // ouverture du burger
            burger.addClass('is-opening');
            body.addClass('no-scroll');
            setTimeout(function () {
                burger.addClass('active').removeClass('is-opening');
            }, 10);

        }
    });

    // DRUPAL - ajout de plus à côté des ul contenant des enfants
    burger_ul.each(function () {
        if ($(this).children().length > 0) {
            $(this).parent().append('<span class="plus"></span>');
        }
    });

    // ouverture des dropdowns
    $('.menu-mobile .plus').on('click', function () {
        var nomParent = $(this).siblings('a').html();
        var hrefParent = $(this).siblings('a').attr('href');
        $(this).siblings('ul').addClass('active');
        burger_header.addClass('active');
        burger_header.find('.conteneur-rappel-menu').html('<a class="rappel-menu"></a>');
        burger_header.find('.rappel-menu').html(nomParent);
        burger_header.find('.rappel-menu').attr('href', hrefParent);
    });

    // Bouton "retour"
    $('.retour-burger').on('click', function () {
        if (burger_ul_4e_nv.hasClass('active')) {
            var nomParent = $('.menu-mobile ul ul.active').parent().find('a').html();
            var hrefParent = $('.menu-mobile ul ul.active').parent().find('a').attr('href');
            burger_header.find('.rappel-menu').html(nomParent);
            burger_header.find('.rappel-menu').attr('href', hrefParent);
            burger_ul_4e_nv.removeClass('active');
        } else if (burger_ul_3e_nv.hasClass('active')) {
            var nomParent = $('.menu-mobile ul.active').parent().find('a').html();
            var hrefParent = $('.menu-mobile ul.active').parent().find('a').attr('href');
            burger_header.find('.rappel-menu').html(nomParent);
            burger_header.find('.rappel-menu').attr('href', hrefParent);
            burger_ul_3e_nv.removeClass('active');
        } else {
            burger_ul.removeClass('active');
            burger_header.removeClass('active');
            burger_header.find('.conteneur-rappel-menu').html('<span class="rappel-menu">Nos rubriques</span>');
        }
    });


    /*** FIN MENU BURGER ***/

    /*** RECHERCHE HEADER ***/

    $('.trigger-recherche').on('click', function () {
        $('.wrapper-nav-recherche').toggleClass('recherche-active');
        $(this).toggleClass('recherche-active');
        $('.recherche input[type="text"]').focus();
    });


    /*** FIN RECHERCHE HEADER ***/


    /*** SLICKS ****/
    // mettez ici tous les slicks du site

    // slider home
    $('.slider-home').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: 4000,
        speed: 300,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 1,
                }
            }
        ]

    });


    /*** exemple de slickification ***/
    // on load (ne pas oublier le resize)
    // if($(window).width() < 1200){
    //     $('.home .actualites-evenements .conteneur-actus').slick({
    //         dots: true,
    //         arrows: false,
    //         infinite: true,
    //         slidesToScroll: 2,
    //         slidesToShow: 2,
    //         autoplay: 4000,
    //         speed: 300,
    //         responsive: [
    //             {
    //                 breakpoint: 992,
    //                 settings: {
    //                     slidesToScroll: 1,
    //                     slidesToShow: 1,
    //                 }
    //             }
    //         ]
    //     });
    // }
    // if($(window).width() > 1200){
    //     if($('.home .actualites-evenements .conteneur-actus').hasClass('slick-initialized')){
    //         $('.home .actualites-evenements .conteneur-actus').slick('unslick');
    //     }
    // }

    // gestion du resize (ne pas oublier le on load)
    // $(window).on('resize', function(){
    //     if($(window).width() < 1200){
    //         if(!$('.home .actualites-evenements .conteneur-actus').hasClass('slick-initialized')){
    //             $('.home .actualites-evenements .conteneur-actus').slick({
    //                 dots: true,
    //                 arrows: false,
    //                 infinite: true,
    //                 slidesToScroll: 2,
    //                 slidesToShow: 2,
    //                 autoplay: 4000,
    //                 speed: 300,
    //                 responsive: [
    //                     {
    //                         breakpoint: 992,
    //                         settings: {
    //                             slidesToScroll: 1,
    //                             slidesToShow: 1,
    //                         }
    //                     }
    //                 ]
    //             });
    //         }
    //     }
    //
    //     if($(window).width() > 1200){
    //         if(($('.home .actualites-evenements .conteneur-actus')).hasClass('slick-initialized')){
    //             $('.home .actualites-evenements .conteneur-actus').slick('unslick');
    //         }
    //     }
    // });

    /*** FIN SLICKS ***/

    /*** SELECT 2 ***/

    // Conteneur filtres - catégorie
    $('select#categorie').select2({
        placeholder: "Catégorie(s)",
        minimumResultsForSearch: Infinity,
    });
    // Conteneur filtres - ville
    $('select#ville').select2({
        placeholder: "Ville(s)",
        minimumResultsForSearch: Infinity,
    });
    // Conteneur filtres - département
    $('select#departement').select2({
        placeholder: "Département(s)",
        minimumResultsForSearch: Infinity,
    });
    // Conteneur filtres - Type de dispositif
    $('select#type').select2({
        placeholder: "Type de dispositif",
        minimumResultsForSearch: Infinity,
    });
    // Conteneur filtres - catégorie
    $('select#public').select2({
        placeholder: "Public(s) accueilli(s)",
        minimumResultsForSearch: Infinity,
    });
    // Conteneur filtres - Type (recherche)
    $('select#type-resultat').select2({
        placeholder: "Type(s) de contenu",
        minimumResultsForSearch: Infinity,
    });

    /*** FIN SELECT 2 ***/

    /*** SIDEBAR ***/

    $('.sidebar .trigger-submenu').on('click', function(){
       $(this).parents('.element').toggleClass('active');
    });

    /*** FIN SIDEBAR ***/

    /*** HOME ***/


    if ($('.page-wrapper.home').length > 0) {
        $('.home').addClass('loaded');
        $window.on('load', function () {
            var windowPos = $window.scrollTop();
            $('.home section').each(function () {
                if ($(this).offset().top < (windowPos + ($window.height() / 1.4))) {
                    $(this).addClass('animated');
                }
            })
        });
        $window.on('scroll', function () {
            var windowPos = $window.scrollTop();
            $('.home section').each(function () {
                if ($(this).offset().top < (windowPos + ($window.height() / 1.4))) {
                    $(this).addClass('animated');
                }
            })
        });
    }

    /*** FIN HOME ***/


    /*** GEM ***/

    // MAP - liste GEM
    if ($('#map-carto-gem').length > 0) {
        var map = L.map('map-carto-gem').setView([51.505, -0.09], 13);

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
        }).addTo(map);

        L.marker([51.5, -0.09]).addTo(map)
    }


    // MAP - détail GEM

    if ($('#map-gem').length > 0) {
        var map = L.map('map-gem').setView([51.505, -0.09], 13);

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
        }).addTo(map);

        L.marker([51.5, -0.09]).addTo(map)
    }


    /*** FIN GEM ***/


    /*** PAGE STANDARD ***/

    // sommaire
    if ($('.contenu-sommaire').length > 0) {
        var sommaire = ($('.contenu-sommaire').height() / 2) + 40;
        $('.contenu-sommaire').css('max-height', sommaire);
    }

    if ($(document).find('.elementor')) {
        $('.wysiwyg').addClass('contenu-elementor');
    }

    /*** FIN PAGE STANDARD ***/

    /*** DICTIONNAIRE DES LIEUX ***/

    if ($('.liste-definitions').length > 0) {

        var alphabet = $('.conteneur-alphabet').offset().top;
        $window.on('scroll', function () {
            if ($window.width() > 991) {
                console.log('sup à 991');
                var windowPos = $window.scrollTop();
                if (windowPos > alphabet) {
                    $('.alphabet').addClass('sticky');
                }
                if (windowPos < alphabet) {
                    $('.alphabet').removeClass('sticky');
                }
            } else {
                $('.alphabet').removeClass('sticky');
                console.log('inf à 991');
            }
        });

        $window.on('resize', function () {
            if ($window.width() < 992) {
                $('.alphabet').removeClass('sticky');
            } else if ($window.width() > 991) {
                var alphabet = $('.conteneur-alphabet').offset().top;

                    var windowPos = $window.scrollTop();
                    if (windowPos > alphabet) {
                        $('.alphabet').addClass('sticky');
                    }
                    if (windowPos < alphabet) {
                        $('.alphabet').removeClass('sticky');
                    }

            }
        });

    }


    /*** FIN DICTIONNAIRE DES LIEUX ***/


});