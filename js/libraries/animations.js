if ($(window).width() > 991){

    //Initialisation des animations
    $('.load-fade, .load-flip, .load-zoom, .load-swipe-l, .load-swipe-r, .load-swipe-t, .load-swipe-b').addClass('--init');

    $(window).scroll(function () {

        //Suppression de la class '--init' afin d'effectuer l'animation
        $('.load-fade, .load-flip, .load-zoom, .load-swipe-l, .load-swipe-r, .load-swipe-t, .load-swipe-b').each(function (e) {
            if ($(this).hasClass('load-swipe-b')){
                var bottom_of_object = $(this).offset().top + $(this).outerHeight() - 500; //Gestion des transform : translateY()

            } else if ($(this).hasClass('load-swipe-t')){
                var bottom_of_object = $(this).offset().top + $(this).outerHeight() + 500; //Gestion des transform : translateY()

            } else {
                var bottom_of_object = $(this).offset().top + $(this).outerHeight() - 150;

            }

            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if (bottom_of_window > bottom_of_object) {
                $(this).removeClass('--init');
            }

        });
    });
}